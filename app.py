import os
import tensorflow as tf
from model_ln import Speech2Text
from data_class import DataSet
import time
from dictionary import dictiony_char_int

tf.logging.set_verbosity(tf.logging.INFO)



char2int,_ = dictiony_char_int()


''' Data directories'''
train_dir = '/tertiary/rizwan/spanish_speech_to_text/tfrecords/train/'
val_dir = '/tertiary/rizwan/spanish_speech_to_text/tfrecords/val'
#test_dir = os.path.join(os.getcwd(),'data/test')



num_classes= len(char2int) + 1


classifier = Speech2Text(num_blocks=3,  num_classes=num_classes, num_features=80, learning_rate=0.0001)
train_input_fn = DataSet(tfrecord=train_dir, batch_size=32, n_epochs=60, num_workers=1, worker_index=0)
eval_input_fn = DataSet(tfrecord=val_dir, batch_size=32, n_epochs=1, num_workers=1, worker_index=0)

# Start training of the network (only for training)
#classifier.fit(train_input_fn)

# Training and evaluation at the same time
classifier.train_and_evaluat(train_input_fn,eval_input_fn)
#classifier.export_model()
