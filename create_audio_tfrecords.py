# -*- coding: utf-8 -*-
import tensorflow as tf
import numpy as np
import os
from glob import glob
from scipy.signal import spectrogram, lfilter, resample_poly
from random import random, shuffle
import soundfile as sf
from tqdm import tqdm
import warnings
from text import normalize_txt_file
import scipy.io.wavfile as wav
import codecs
from dictionary import dictiony_char_int
from concurrent.futures import ProcessPoolExecutor
import time
from functools import partial
from random import random
from math import ceil

char2int,_ = dictiony_char_int()



def read_spanish_file(file):
    with codecs.open(file,'r','utf-8') as f:
        line = f.readlines()[0]
    return line

def rm_dc_n_dither(sin,fs=8000):
    alpha = 0.999
    sin = lfilter([1,-1],[1,-alpha],sin) # remove DC
    dither = np.random.rand(np.size(sin))+np.random.rand(np.size(sin))-1
    s_pow = np.std(sin)
    sout = sin + 1e-6*s_pow*dither
    return sout


def map_function(file):
    s, fs = sf.read(file)
    if s.ndim>1:
        s = s[:,0]
    s = s/np.max(np.abs(s))
    if fs!=8000:
        s = resample_poly(s,8000,fs)
        s = s/np.max(np.abs(s))
        wav.write(file,8000, (s*0x8000).astype('int16'))

    s = rm_dc_n_dither(s,fs)
    s = s/np.max(np.abs(s))
    _,_, Sxx = spectrogram(s,fs=8000, nperseg=int(25/1000*8000), noverlap=int(10/1000*8000), nfft=1024)
    labels = read_spanish_file(file.replace('.wav','.txt'))
    labels = np.array([char2int.get(i, None) for i in labels.lower() if char2int.get(i, None) != None])
    return s.astype('float32'), labels






def _wrap_int64(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def _wrap_bytes(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _wrap_float(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def audio_to_tf_record(files, type):
    curr_time = '{}'.format(time.time()).replace('.','')
    file_name = '/tertiary/rizwan/spanish_speech_to_text/tfrecords/{}/{}_{}.tfrecords'.format(type,type,curr_time)
    with tf.python_io.TFRecordWriter(file_name) as writer:
        for file in files:
            warnings.filterwarnings('error')
            try:
                audio_data, labels = map_function(file)
                if not(len(labels)):
                    print(len(labels))
                    continue
                data = {
                    'audio': _wrap_float(audio_data),
                    'labels': _wrap_int64(labels)
                }
            except Exception as e:
                with open('file.txt','a') as f:
                    f.write(file)
                    f.write('\n')
                print(e)
                continue
            except RuntimeWarning as e:
                print(e)
                continue

            # Wrap the data as TensorFlow Features.
            feature = tf.train.Features(feature=data)

            # Wrap again as a TensorFlow Example.
            example = tf.train.Example(features=feature)

            # Serialize the data.
            serialized = example.SerializeToString()

            # Write the serialized data to the TFRecords file.
            writer.write(serialized)








## Training Files
## Training Files
training_files = sorted(glob(os.path.join('/tertiary/rizwan/spanish_speech_to_text/Noise_augmentation','*.wav')), key=lambda *args: random())
print(len(training_files))
training_files.extend(sorted(glob(os.path.join('/tertiary/rizwan/spanish_speech_to_text/IR_augmentation','*.wav')), key=lambda *args: random()))
print(len(training_files))
training_files.extend(sorted(glob(os.path.join('/tertiary/rizwan/spanish_speech_to_text/pertubation_agumentation','*.wav')), key=lambda *args: random()))
print(len(training_files))
training_files.extend(sorted(glob(os.path.join('/tertiary/rizwan/spanish_speech_to_text/speech_to_text','*.wav')), key=lambda *args: random()))
print(len(training_files))
training_files.extend(sorted(glob(os.path.join('/tertiary/rizwan/spanish_speech_to_text/music_augmentation','*.wav')), key=lambda *args: random()))

shuffle(training_files)
print(len(training_files))




n_files_ = 3000   # number of files in one tf.records
n_div = ceil(len(training_files)/n_files_) # number of tf records
file_list = [training_files[index*n_files_:min((index+1)*n_files_,len(training_files))] for index in range(n_div)]

with ProcessPoolExecutor() as ex:
    ex.map(partial(audio_to_tf_record,type='train'), file_list)



## Validataion Files
validation_files = sorted(glob(os.path.join('/tertiary/rizwan/spanish_speech_to_text/speech_to_text_no_clean','*.wav')), key=lambda *args: random())
n_files_ = 3000   # number of files in one tf.records
n_div = ceil(len(validation_files)/n_files_) # number of tf records
file_list = [validation_files[index*n_files_:min((index+1)*n_files_,len(validation_files))] for index in range(n_div)]


with ProcessPoolExecutor() as ex:
    ex.map(partial(audio_to_tf_record,type='val'), file_list)
