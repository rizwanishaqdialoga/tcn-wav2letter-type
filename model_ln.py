# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import tensorflow as tf
from utils import CBHG
from dictionary import dictiony_char_int
from gated_residual_network import res_network

tf.logging.set_verbosity(tf.logging.INFO)











def network(features, mode, params):


    # Default parameters
    num_layers = params['num_blocks']
    num_classes = params['num_classes']
    keep_prob = params['keep_prob']
    is_training = mode==tf.estimator.ModeKeys.TRAIN

    outputs  = res_network(features['spectrogram'], params)

    fc_1 = tf.layers.conv1d(outputs, 
                            filters=512, 
                            kernel_size=1, 
                            strides=1, 
                            padding='same', 
                            dilation_rate=1, 
                            activation=tf.nn.elu,
                            kernel_initializer=tf.contrib.layers.xavier_initializer())
    fc_1 = tf.nn.dropout(fc_1, keep_prob=keep_prob if is_training==True else 1.)
    fc_2 = tf.layers.conv1d(fc_1, 
                            filters=256, 
                            kernel_size=1, 
                            strides=1, 
                            padding='same', 
                            dilation_rate=1, 
                            activation=tf.nn.elu,
                            kernel_initializer=tf.contrib.layers.xavier_initializer())
    fc_2 = tf.nn.dropout(fc_2, keep_prob=keep_prob if is_training==True else 1.)
 
   
    logits= tf.layers.conv1d(fc_2, 
                            filters=num_classes, 
                            kernel_size=1, 
                            strides=1, 
                            padding='same', 
                            dilation_rate=1, 
                            kernel_initializer=tf.contrib.layers.xavier_initializer(),
                            activation=None)
    
    logits = tf.transpose(logits,[1,0,2])

    return logits



def optimizer_fn(cost, initial_learning_rate):
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    opt = tf.train.AdamOptimizer(initial_learning_rate)
    with tf.control_dependencies(update_ops):
        train_optimizer = opt.minimize(cost, global_step=tf.train.get_global_step())
  
    return train_optimizer

def loss_fn(logits, labels, seq_lens):
    loss = tf.nn.ctc_loss(labels, logits, seq_lens, ignore_longer_outputs_than_inputs=True)
    ctc_cost = tf.reduce_mean(loss)
    tf.losses.add_loss(ctc_cost)
    cost = tf.losses.get_total_loss()


    return cost

def config_fn():
    per_process_gpu_memory_fraction = 1.0 / 1.0
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=per_process_gpu_memory_fraction,
                                allow_growth=True)
    tf_config = tf.ConfigProto(gpu_options=gpu_options,
                               allow_soft_placement=True)
    config = tf.estimator.RunConfig(save_summary_steps=100,
                                    save_checkpoints_steps=100,
                                    session_config=tf_config,
                                    keep_checkpoint_every_n_hours=1,
                                    log_step_count_steps=100,
                                    keep_checkpoint_max=50)
    return config

def model_fn(features, labels, mode, params):
    logits = network(features, mode, params)
    #list_of_variable = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
    # Network
    decoded, log_prob = tf.nn.ctc_greedy_decoder(logits, sequence_length=features['seq_lens'])
    #decoded, log_prob = tf.nn.ctc_beam_search_decoder(logits, sequence_length=features['seq_lens'])
    # decoding
    decoded = tf.to_int32(decoded[0])
    preds = tf.sparse_tensor_to_dense(decoded, default_value=-1)
    predictions = {'preds': preds, 'logit':logits, 'shape':tf.shape(logits)}
    # Prediction

    if mode == tf.estimator.ModeKeys.PREDICT:
        export_outputs = {
            tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY:
            tf.estimator.export.PredictOutput(outputs=predictions)}
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions, export_outputs= export_outputs)
        #return tf.estimator.EstimatorSpec(mode=mode)

    labels = tf.contrib.layers.dense_to_sparse(labels,eos_token=500)
    loss = loss_fn(logits,labels,features['seq_lens'])
    tf.summary.scalar('loss', loss)
    edit_dist_op = tf.reduce_mean(tf.edit_distance(decoded, labels))
    tf.summary.scalar('edit_dist', edit_dist_op)
    lth = tf.train.LoggingTensorHook({'edit_dist': edit_dist_op}, every_n_iter=100)
    if mode == tf.estimator.ModeKeys.TRAIN:
        train_op = optimizer_fn(loss,params['learning_rate'])

        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op, training_hooks=[lth])

    if mode == tf.estimator.ModeKeys.EVAL:
        eval_metric_ops = {'edit_dist': tf.metrics.mean(edit_dist_op)}
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, eval_metric_ops=eval_metric_ops, evaluation_hooks=[lth])





def serving_input_receiver_fn():
    def _spectrogram(reciever_tensors, frame_length=200, frame_step=120, fft_length=1024, sample_rate=8000, log_offset=1e-12):
        stft = tf.contrib.signal.stft(reciever_tensors['audio'],
                                  frame_length=frame_length,
                                  frame_step=frame_step,
                                  fft_length=fft_length,
                                  window_fn=tf.contrib.signal.hann_window)
        magnitude_spectrograms = tf.abs(stft)
        linear_to_mel_weight_matrix = tf.contrib.signal.linear_to_mel_weight_matrix(num_mel_bins=80, 
                                                                                    num_spectrogram_bins=513,
                                                                                    sample_rate=sample_rate,
                                                                                    lower_edge_hertz=125.0,
                                                                                    upper_edge_hertz=3800.0)
        mel_spectrograms = tf.tensordot(magnitude_spectrograms, linear_to_mel_weight_matrix, 1)
        mel_spectrograms.set_shape(magnitude_spectrograms.shape[:-1].concatenate(linear_to_mel_weight_matrix.shape[-1:]))
        log_mel_spectrograms = tf.div(tf.log(mel_spectrograms+log_offset),tf.log(tf.constant(10.,dtype=tf.float32)))
        b_mean,b_variance = tf.nn.moments(log_mel_spectrograms,axes=[-2])
        log_mel_spectrograms = tf.nn.batch_normalization(x=log_mel_spectrograms,mean=b_mean,variance=b_variance, offset=None, scale=None, variance_epsilon=1e-12)
        seq_lens = tf.shape(log_mel_spectrograms)[-2]
        return log_mel_spectrograms, seq_lens
    """
    This is used to define inputs to serve the model
    """
    receiver_tensors = {
        'audio': tf.placeholder(tf.float32, [None,None])
    }
    spectrogram, seq_lens = _spectrogram(receiver_tensors)
    features = {
        'spectrogram': spectrogram,
        'seq_lens': seq_lens
    }


    return tf.estimator.export.ServingInputReceiver(receiver_tensors=receiver_tensors,
                                                    features=features)




class Speech2Text():
    def __init__(self, num_blocks, num_classes, num_features, learning_rate, kernel_size=15, dilation_rate=[1,2,4,16,32]):
        params = {
            'num_blocks': num_blocks,
            'num_classes': num_classes,
            'num_features':num_features,
            'kernel_size': kernel_size, 
            'dilation_rate': dilation_rate,
            'learning_rate':learning_rate,
            'keep_prob':0.95,
    
        }
        config = config_fn()
        #self.model = tf.estimator.Estimator(model_fn=model_fn,params=params,model_dir='hdfs://hadoop-cluster-dialoga/users/rizwan/speech_2_text/model/',config=config)
        #latest_ckp = tf.train.latest_checkpoint('./warm_from')
        #reader = pywrap_tensorflow.NewCheckpointReader(latest_ckp)
        #var_to_shape_map = reader.get_variable_to_shape_map()
        #print(len(var_to_shape_map))
        #del(var_to_shape_map['fully_connected_1/weights'])
        #del(var_to_shape_map['fully_connected_1/biases/Adam'])
        #print(len(var_to_shape_map))
       # ws = tf.estimator.WarmStartSettings(ckpt_to_initialize_from="./warm_from", 
                                            #vars_to_warm_start='^(bidirectional_rnn.*|conv1d.*|prenet.*|highwaynet.*)')
        #ws = tf.estimator.WarmStartSettings(ckpt_to_initialize_from="./warm_from")
        #self.model = tf.estimator.Estimator(model_fn=model_fn,params=params,model_dir='./model',config=config, warm_start_from= ws)
        self.model = tf.estimator.Estimator(model_fn=model_fn,params=params,model_dir='./model',config=config)


    def fit(self, train_input_fn):
        self.model.train(input_fn=train_input_fn)

    def evaluate(self, eval_input_fn):
        eval_results = self.model.evaluate(input_fn=eval_input_fn)
        return eval_results

    def train_and_evaluat(self,train_input_fn,eval_input_fn):
        train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn)
        eval_spec = tf.estimator.EvalSpec(input_fn=eval_input_fn,  start_delay_secs = 60*60)
        tf.estimator.train_and_evaluate(self.model, train_spec, eval_spec)

    def export_model(self):
        self.model.export_savedmodel('./pb', serving_input_receiver_fn=serving_input_receiver_fn)


    
