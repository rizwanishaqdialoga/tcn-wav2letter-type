import os
from glob import glob
import tensorflow as tf
import functools
from scipy.signal import spectrogram

def data_input_fn(tfrecord, worker_index, num_workers, batch_size=10,n_epochs=1):
    #char2ind = tf.constant([" ","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"])
    #table = tf.contrib.lookup.index_table_from_tensor(mapping=char2ind,num_oov_buckets=1, default_value=-1)


    def _spectrogram(audio,label, frame_length=200, frame_step=120, fft_length=1024, sample_rate=8000, log_offset=1e-12):
        ''' The parsing function which return the spectrogram and labels'''
        ''' The spectrogram is calculated with sampling frequency of 8000 Hz and frame_length of 25 msec with 15 msec frame_step'''
        '''' It is similar to scipy.spectrogram where frame_length=200(25 msec) and frame_step=80(10 msec) instead of 120'''

        stft = tf.contrib.signal.stft(audio,
                                  frame_length=frame_length,
                                  frame_step=frame_step,
                                  fft_length=fft_length,
                                  window_fn=tf.contrib.signal.hann_window)
        magnitude_spectrograms = tf.abs(stft)
        linear_to_mel_weight_matrix = tf.contrib.signal.linear_to_mel_weight_matrix(num_mel_bins=80, 
                                                                                    num_spectrogram_bins=513,
                                                                                    sample_rate=sample_rate,
                                                                                    lower_edge_hertz=125.0,
                                                                                    upper_edge_hertz=3800.0)
        mel_spectrograms = tf.tensordot(magnitude_spectrograms, linear_to_mel_weight_matrix, 1)
        mel_spectrograms.set_shape(magnitude_spectrograms.shape[:-1].concatenate(linear_to_mel_weight_matrix.shape[-1:]))
        log_mel_spectrograms = tf.div(tf.log(mel_spectrograms+log_offset),tf.log(tf.constant(10.,dtype=tf.float32)))
        b_mean,b_variance = tf.nn.moments(log_mel_spectrograms,axes=[-2])
        log_mel_spectrograms = tf.nn.batch_normalization(x=log_mel_spectrograms,mean=b_mean,variance=b_variance, offset=None, scale=None, variance_epsilon=1e-12)
        seq_lens = tf.shape(log_mel_spectrograms)[-2]
        return log_mel_spectrograms, tf.cast(label,tf.int32), seq_lens


    def _parse_final(spectrogram, label, seq_lens):
        features = {'spectrogram': spectrogram, 'seq_lens':seq_lens}
        return features, label


    def _parse_record(tf_record):
        features = {'audio': tf.FixedLenSequenceFeature([], tf.float32,allow_missing=True),
                            'labels': tf.FixedLenSequenceFeature([], dtype=tf.int64, allow_missing=True)}
        # Parse the serialized data so we get a dict with our data.
        parsed_example = tf.parse_single_example(serialized=tf_record, features=features)
        audio = parsed_example['audio']
        label = parsed_example['labels']

        return audio, label
    files = tf.data.Dataset.list_files(tfrecord)
    dataset = files.apply(tf.contrib.data.parallel_interleave(
                    lambda filename: tf.data.TFRecordDataset(filename,buffer_size=32*1024*1024, num_parallel_reads=8),
                    cycle_length=8,
                    sloppy=True))

    
    dataset = dataset.apply(tf.contrib.data.shuffle_and_repeat(batch_size, n_epochs))
    dataset = dataset.map(_parse_record, num_parallel_calls=8)
    dataset = dataset.map(_spectrogram, num_parallel_calls=8)
    dataset = dataset.padded_batch(batch_size,([None,80],[None],[]), padding_values=(0., tf.constant(500,dtype=tf.int32), tf.constant(500,dtype=tf.int32)))
    dataset = dataset.map(_parse_final,num_parallel_calls=8)
    dataset = dataset.prefetch(batch_size)
    return dataset


class DataSet:
    def __init__(self, tfrecord, batch_size, n_epochs, num_workers, worker_index):
        self.tfrecord = os.path.join(tfrecord,'*.tfrecords')
        self.num_workers = num_workers
        self.worker_index = worker_index
        self.batch_size = batch_size
        self.n_epochs = n_epochs

    def __call__(self):
        return data_input_fn(self.tfrecord,self.worker_index, self.num_workers, batch_size=self.batch_size,n_epochs=self.n_epochs)

if __name__ == '__main__':
    pass
