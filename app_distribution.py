import os
import tensorflow as tf
from model_ln import LanguageIdentifier
from data_class import DataSet
import json


'''                     How to run the code             '''
'''  python3 app_distribution.py --type='ps' --index=0                                                    '''







'''             #CLUSTER SPECIFICATION#       '''
cluster = {'chief': ['100.100.100.72:2224'],
            'ps':['100.100.100.72:2225'],
            'worker':['100.100.100.74:2222','100.100.100.75:2222']
}



tf.app.flags.DEFINE_string('type','ps','type of task(ps/worker/chief)')
tf.app.flags.DEFINE_integer('index',0,'task id')
tf.app.flags.DEFINE_integer('num_workers',3,'number of workers')
FLAGS = tf.flags.FLAGS

task_type = FLAGS.type
task_index = FLAGS.index
num_workers = FLAGS.num_workers


os.environ['TF_CONFIG'] = json.dumps({'cluster':cluster, 'task':{'type':task_type, 'index':task_index}})
if task_type == 'ps':
    os.environ['CUDA_VISIBLE_DEVICES'] = ''










'''             Main code           '''
tf.logging.set_verbosity(tf.logging.INFO)

''' Data directories'''
train_dir = 'hdfs://hadoop-cluster-dialoga/users/rizwan/language/train.tfrecords'
val_dir = 'hdfs://hadoop-cluster-dialoga/users/rizwan/language/train.tfrecords'
test_dir = 'hdfs://hadoop-cluster-dialoga/users/rizwan/language/train.tfrecords'


classifier = LanguageIdentifier(rnn_size=256, num_layers=2, num_classes=7, num_features=513, learning_rate=0.0001)
train_input_fn = DataSet(tfrecord=train_dir, batch_size=40, n_epochs=50, num_workers=num_workers, worker_index=task_indextask_index)
eval_input_fn = DataSet(tfrecord=val_dir, batch_size=40, n_epochs=1, num_workers=num_workers, worker_index=task_index)

# Start training of the network (only for training)
#classifier.fit(train_input_fn)

# Training and evaluation at the same time
classifier.train_and_evaluat(train_input_fn,eval_input_fn)
