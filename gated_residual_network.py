import tensorflow as tf

def residual_cnn_block(input_x, dilation_rate, params, is_training=True):
    ## Some updation, I have done, to have the future context (i.e. without padding)
    #x = tf.contrib.layers.layer_norm(input_x,center=True,scale=True)
    x = tf.layers.batch_normalization(input_x, training=is_training)
    x = tf.layers.conv1d(inputs=x, filters=64, kernel_size = 1, padding='same')
    x = tf.nn.elu(x)

    #pad_sz = (params['kernel_size'] - 1) * dilation_rate
    #pad = tf.zeros([tf.shape(x)[0], pad_sz, tf.shape(x)[-1]])
    #x = tf.concat([pad, x, pad],1)
    dilated_conv_linear =  tf.layers.conv1d(inputs = x, filters = 64,kernel_size = params['kernel_size'], dilation_rate = dilation_rate, padding='same')
    dilated_conv_sigmoid =  tf.layers.conv1d(inputs = x, filters = 64,kernel_size = params['kernel_size'], dilation_rate = dilation_rate,  padding='same')
    dilated_conv_sigmoid = tf.nn.sigmoid(dilated_conv_sigmoid)

    x = dilated_conv_linear * dilated_conv_sigmoid
    x = tf.layers.conv1d(inputs=x, filters=256, kernel_size = 1, padding='same')
    #x = x[:, :-pad_sz, :]

    skip = x
    out = input_x + x
    out = tf.nn.elu(out)
    return skip, out


def res_network(features, params):
      # expand the channel dimension for time-frequency-convolution(conv2d)
  with tf.variable_scope('dim_expand'):
    conv_input_a = tf.expand_dims(features, -1)

  #Frequency-dilated module
  with tf.variable_scope('freq-conv2d'):
    conv2d_1 = tf.layers.conv2d(conv_input_a, filters=16, kernel_size=(35,5), dilation_rate=[1,1], padding='same')
    conv2d_2 = tf.layers.conv2d(conv2d_1, filters=16, kernel_size=(25,5), dilation_rate=[1,1], padding='same')
    conv2d_3 = tf.layers.conv2d(conv2d_2, filters=32, kernel_size=(15,5), dilation_rate=[1,2], padding='same')
    conv2d_4 = tf.layers.conv2d(conv2d_3, filters=32, kernel_size=(5,5), dilation_rate=[1,4], padding='same')

    out = tf.reshape(conv2d_4, [-1, tf.shape(conv2d_4)[1], params['num_features']*32])

  with tf.variable_scope('conv1d_1'):
    out = tf.layers.conv1d(inputs=out, filters=256, kernel_size = 1, padding='same')


  # Time-dilated module
  skip_n = []
  with tf.variable_scope('time-conv1d'):
    for block in range(params['num_blocks']):
      for i in params['dilation_rate']:
        with tf.variable_scope('time-dilated_{}-block_{}'.format(i,block)):
          skip, out = residual_cnn_block(out, i, params)
        skip_n.append(skip)

  out = tf.add_n(skip_n) + out


  return out
